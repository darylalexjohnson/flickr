import Ember from 'ember';
import { task, timeout } from 'ember-concurrency';

export default Ember.Component.extend({

  flickr: Ember.inject.service(),
  location: Ember.inject.service(),
  photos: null,

  curentLocation: task(function *() {
    let location = this.get('location');
    try {
      while (true) {
        if (!location.get('here')) {
          this.get('search').perform();
          location.getGeolocation();
        }
        yield timeout(60000);
      }
    } catch(e) {
      console.log('There is an error with your browsers geolocation.', e);
    }
  }).on('init'),

  search: task(function *(event = null) {
    let location = this.get('location');

    let params = {
      tags: event === null ? '' : event.target.value,
      lat: location.get('currentLat'),
      lon: location.get('currentLong')
    };

    yield timeout(500);
    let results = yield this.get('flickr').search(params);
    this.set('photos', results);
    location.setProperties({
      'latitude': location.get('currentLat'),
      'longitude': location.get('currentLong')
    });
  }).restartable()
});
