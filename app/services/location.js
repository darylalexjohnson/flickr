import Ember from 'ember';
import { task, timeout } from 'ember-concurrency';

export default Ember.Service.extend({

  latitude: null,
  longitude: null,
  currentLat: null,
  currentLong: null,

  latChanged: Ember.computed('latitude', 'currentLat', function() {
    return this.get('latitude') === this.get('currentLat');
  }),

  longChanged: Ember.computed('longitude', 'currentLong', function() {
    return this.get('longitude') === this.get('currentLong');
  }),

  here: Ember.computed('latChanged', 'longChanged', function() {
    return this.get('latChanged') && this.get('longChanged');
  }),

  getGeolocation() {
    let geolocation = new Ember.RSVP.Promise(function(resolve, reject){
      navigator.geolocation.getCurrentPosition(resolve, reject)
    });

    return geolocation.then(position => {
      this.setProperties({
        currentLat: position.coords.latitude,
        currentLong: position.coords.longitude
      });
    });
  }

});
