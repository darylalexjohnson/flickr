import Ember from 'ember';
import $ from 'jquery';

export default Ember.Service.extend({
  init() {
    this._super(...arguments);
    this._photoCache = {};
  },

  reset() {
    this._photoCache = {};
  },

  search(params) {
    return this.apiCall('flickr.photos.search', params).then(res => {
      if (res.stat === "fail") {return;}
      let photos = res.photos.photo;
      photos.forEach(photo => {
        this._photoCache[photo.id] = photo;
      });
      return photos;
    });
  },

  findPhoto(photoId) {
    let cachedPhoto = this._photoCache[photoId];
    if (cachedPhoto) {
      return Ember.RSVP.resolve(cachedPhoto);
    } else {
      return this.apiCall('flickr.photos.getInfo', {photo_id: photoId}).then(res => { return res.photo; });
    }
  },
  
  apiCall(method, params) {
    let queryParams = this.buildParams(params);
    let apiKey = '18f07afc15c9c8755e4394e0c9cdbf32';
    let url = `https://api.flickr.com/services/rest/?method=${method}&api_key=${apiKey}&format=json&nojsoncallback=1`;
    url += '&' + $.param(queryParams);
    return Ember.RSVP.resolve($.getJSON(url));
  },

  buildParams(params) {
    for (var key in params) {
      if (params.hasOwnProperty(key) && params[key] === null) {
        delete params[key];
      }
    }
    return params;
  }
});
