import Ember from 'ember';
import { task, timeout } from 'ember-concurrency';

export default Ember.Route.extend({
  location: Ember.inject.service(),

  curentLocation: task(function *() {
    let location = this.get('location');
    return yield location.getGeolocation();
  }),

  beforeModel() {
    //TODO::Need an app loading state while this promise resolves
    //TODO:: Check that navigation is enabled on browser.
    return this.get('curentLocation').perform();
  }

});
