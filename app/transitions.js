export default function() {
  this.transition(
    this.fromRoute('search'),
    this.toRoute('photo'),
    this.use('explode', {
      matchBy: 'data-photo-id',
      use: ['fly-to', {duration: 1200}]
    }),
    this.reverse('explode', {
      matchBy: 'data-photo-id',
      use: ['fly-to', {duration: 1200}]
    }),
    this.debug()
  );
}
