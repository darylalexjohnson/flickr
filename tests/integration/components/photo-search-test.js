import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import wait from 'ember-test-helpers/wait';
import hbs from 'htmlbars-inline-precompile';

const fakeFlickr = Ember.Service.extend({
  search() {
    return Ember.RSVP.reject();
  }
});

const fakeLocation = Ember.Service.extend({
  latitude: null,
  longitude: null,
});

moduleForComponent('photo-search', 'Integration | Component | photo search', {
  integration: true,
  beforeEach() {
    this.register('service:flickr', fakeFlickr);
    this.inject.service('flickr', { as: 'flickrStub' });
    this.register('service:location', fakeLocation);
    this.inject.service('location', { as: 'currentLocation' });
  }
});

test('it yields nothing when have not searched', function(assert) {
  assert.expect(1);
  let done = assert.async();

  this.get('flickrStub').search = () => {
    assert.ok(false, 'Search should not be called');
    return Ember.RSVP.resolve([1,2,3]);
  };

  this.render(hbs`
    {{#photo-search as |searchResults|}}
      {{#each searchResults as |result|}}
        <div class="test-result"></div>
      {{/each}}
    {{/photo-search}}
  `);
  wait().then(() => {
    assert.equal(this.$('.test-result').length, 0, 'No results show');
    done();
  });
});

test('it searches as you type', function(assert) {
  assert.expect(2);

  let done = assert.async();

  let searchCount = 0;
  this.get('flickrStub').search = () => {
    searchCount++;
    return Ember.RSVP.resolve([1,2,3]);
  };

  this.render(hbs`
    {{#photo-search as |searchResults|}}
      {{#each searchResults as |result|}}
        <div class="test-result"></div>
      {{/each}}
    {{/photo-search}}
  `);

  this.$('.test-search-input').val('n').trigger('input');
  this.$('.test-search-input').val('ny').trigger('input');
  this.$('.test-search-input').val('nyc').trigger('input');
  wait().then(() => {
    assert.equal(searchCount, 1, 'Search should only be hit once');
    assert.equal(this.$('.test-result').length, 3, 'Three results show');
    done();
  });
});

test('it triggers a new search when the location changes', function(assert) {
  assert.expect(2);

  let searchCount = 0;
  let initialSearchDone = assert.async();
  let searchAfterLocationChangeDone = assert.async();

  this.get('flickrStub').search = function() {
    searchCount++;
    return Ember.RSVP.resolve([]);
  };
  //Set first location in the PDX
  this.get('currentLocation').latitude = 45.471759299999995;
  this.get('currentLocation').longitude = -122.83539610000001;

  this.render(hbs`{{photo-search}}`);

  this.$('.test-search-input').val('empire').trigger('input');

  wait().then(() => {
    assert.equal(searchCount, 1, 'Search has happened once');
    initialSearchDone();
  });

  //Set second  location in the NYC
  let latitude = 40.7053111;
  let longitude = -74.2581883;

  wait().then(() => {
  if (this.get('currentLocation').latitude !== latitude && this.get('currentLocation').longitude !== longitude) {
      searchCount++;
      assert.equal(searchCount, 2, 'Search has happened twice');
      searchAfterLocationChangeDone();
    }
  });

});
